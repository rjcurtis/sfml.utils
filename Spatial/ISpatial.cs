﻿using System;
using SFML.Graphics;

namespace SFML.Utils.Spatial
{
    public interface ISpatial
    {
        FloatRect AABB { get; }
        event Action<ISpatial> AABBChanged;
    }
}